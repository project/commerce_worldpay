## CONTENTS OF THIS FILE

### Worldpay Integration (Direct).

* Create a payment gateway Direct.
* Use the secret key Details from the Worldpay logged in account settings.
* Use the account in test account settings to run transactions.

### Worldpay Integration (Redirect).

* Create a payment gateway.
* In Worldpay set the Installation instructions. For this module to work properly you must configure a few specific options in your RBS WorldPay account under Installation Administration settings:
  * Payment Response URL must be set to: `<wpdisplay item=MC_callback-ppe empty="http://yoursitename/payment/notify/MACHINE_NAME_OF_PAYMENT_GATEWAY"/>;`
  * "Payment Response enabled?" must be enabled
  * Enable the Shopper Response should be enabled to get the Commerce response page.
  * Shopper Redirect URL and set the value to be `<wpdisplay item=MC_callback/>`.
  * SignatureFields must be set to: `instId:amount:currency:cartId:MC_orderId:MC_callback`
* Set the in the payment plugin the Installation ID, Site ID, Installation password. These are found on the worldpay account.
* The secret key is a hash key you will need to generate this your self using something like https://www.md5hashgenerator.com/
* The resultC.html & resultY.html do not need to be uploaded to Worldpay site as we are using the templates located at commerce_worldpay/templates

API Docs: https://developer.worldpay.com/jsonapi/docs/
Shopping Carts: https://business.worldpay.com/shopping-carts

Internal (API): https://business.worldpay.com/developer-support/direct-integration
External (iFrame & redirect): https://business.worldpay.com/developer-support/redirect-integration

## Testing

To test your implementation, add a product to your cart, proceed thorough checkout
and enter a credit card. We recommend using a sandbox account for this before
attempting to go live. If you're using a sandbox account, you can use the following
credit card to test a transaction.

###### Visa Card

Credit Card #: 4444333322221111
Expiration Date: Any 4 digit expiration greater than today's date
CVC (if enabled): Any 3 digit code

###### Master Card

Credit Card #: 5555555555554444
Expiration Date: Any 4 digit expiration greater than today's date
CVC (if enabled): Any 3 digit code

###### American Express

Credit Card #: 34343434343434
Expiration Date: Any 4 digit expiration greater than today's date
CVC (if enabled): Any 3 digit code

###### Discover

Credit Card #: 6011000400000000
Expiration Date: Any 4 digit expiration greater than today's date
CVC (if enabled): Any 3 digit code

### Note about credit card types

As per Worldpay documentation found in here - https://merchants.worldpay.us/docs/index.taf?topic=faq
WorldPay Online Commerce Suite can process VISA, MasterCard, American Express,
Discover, Diners Club and JCB. In order to process Discover credit cards, you need
to apply with Discover for a Discover Merchant account. Once the account is approved,
you must provide the information to your Merchant Bank that processes your MasterCard
and VISA transactions. After this is confirmed, the final step is to login to
Online Merchant Center, open the Acct Edit/Delete menu, and select the payment option Discover.
