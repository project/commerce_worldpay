<?php

namespace Drupal\commerce_worldpay\PluginForm\Onsite;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class WorldpayDirectForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $plugin = $this->plugin;
    // Display credit card logos in checkout form.
    if ($plugin->getConfiguration()['enable_credit_card_icons']) {
      $form['#attached']['library'][] = 'commerce_worldpay/credit_card_icons';
      $form['#attached']['library'][] = 'commerce_payment/payment_method_icons';

      $supported_credit_cards = [];
      foreach ($plugin->getCreditCardTypes() as $credit_card) {
        $supported_credit_cards[] = $credit_card->getId();
      }

      $form['credit_card_logos'] = [
        '#theme' => 'commerce_worldpay_credit_card_logos',
        '#credit_cards' => $supported_credit_cards,
      ];
    }

    return $form;

  }

}
